FROM node:14.15.4 as development
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:14.15.4-alpine as production
LABEL maintainer="Roberto Galaz <galaz.becerra@gmail.com>"
ENV NODE_ENV production
ENV PORT 5000
WORKDIR /usr/src/app
RUN apk add --no-cache bash
COPY --from=development /usr/src/app/node_modules ./node_modules
COPY --from=development /usr/src/app/package.json /usr/src/app/dist ./
COPY ./entrypoint.sh /usr/local/bin/
RUN npm prune --production
ENTRYPOINT ["entrypoint.sh"]
CMD ["node", "main"]
